PACKAGE := docker-qemu
TAG := $(shell git tag | sort -r |head -n 1 )
NAME = jlcox1970/$(PACKAGE)

ifeq ($(shell git tag -l --points-at HEAD |wc -l) , 0)
        PATCH := $(shell echo ${TAG} |cut -d- -f1 |cut -d. -f3 )
        PAST_TAG := $(shell echo ${TAG} |cut -d- -f2)
        NEW_PATCH := $(shell echo $$(( $(PATCH) + $(PAST_TAG) )) )
        BASE := $(shell echo ${TAG} |cut -d- -f1 |cut -d. -f1,2 |sed 's/^v//g' )
        VERSION := $(shell echo "${BASE}.${NEW_PATCH}" )
else
        VERSION := $(shell echo ${TAG} )
endif

build:
	docker build -t $(NAME):$(TAG) src --load

build-nocache:
	docker build -t $(NAME):$(TAG) --no-cache src --load




tag-latest:
	docker tag $(NAME):$(TAG) $(NAME):latest

push: build
	docker push $(NAME):$(TAG)

push-latest:
	docker push $(NAME):latest

release: build tag-latest push push-latest

git-tag-version: release
	git tag -a v$(VERSION) -m "v$(VERSION)"
	git push origin v$(VERSION)
	git push --tags
