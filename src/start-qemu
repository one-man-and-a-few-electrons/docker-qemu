#!/bin/bash
set -e

# main available options:
#   QEMU_CPU=n    (cores)
#   QEMU_RAM=nnn  (megabytes)
#   QEMU_HDA      (filename)
#   QEMU_HDA_SIZE (bytes, suffixes like "G" allowed)
#   QEMU_CDROM    (filename)
#   QEMU_BOOT     (-boot)
#   QEMU_PORTS="xxx[ xxx ...]" (space separated port numbers)
#   QEMU_NET_USER_EXTRA="net=192.168.76.0/24,dhcpstart=192.168.76.9" (extra raw args for "-net user,...")
#   QEMU_NO_SSH=1 (suppress automatic port 22 forwarding)
#   QEMU_NO_SERIAL=1 (suppress automatic "-serial stdio")
#   QEMU_HOSTNAME  (hostname for cloud-init)
#   QEMU_OS        ( OS for guest)

cloud-init ${QEMU_HOSTNAME} ${QEMU_OS}


hostArch="$(uname -m)"
qemuArch="${QEMU_ARCH:-$hostArch}"
qemu="${QEMU_BIN:-qemu-kvm}"
#qemu="${QEMU_BIN:-qemu-system-$qemuArch}"
qemuArgs=()

qemuPorts=()
if [ -z "${QEMU_NO_SSH:-}" ]; then
	qemuPorts+=( 22 )
fi
qemuPorts+=( ${QEMU_PORTS:-} )

if [ -e /dev/kvm ]; then
	qemuArgs+=( -enable-kvm )
elif [ "$hostArch" = "$qemuArch" ]; then
	echo >&2
	echo >&2 'warning: /dev/kvm not found'
	echo >&2 '  PERFORMANCE WILL SUFFER'
	echo >&2 '  (hint: docker run --device /dev/kvm ...)'
	echo >&2
	sleep 3
fi

qemuArgs+=( -smp "${QEMU_CPU:-2}" )
qemuArgs+=( -m "${QEMU_RAM:-2048}" )

if [ -n "${QEMU_HDA:-}" ]; then
	if [ ! -f "$QEMU_HDA" -o ! -s "$QEMU_HDA" ]; then
		(
			set -x
			qemu-img create -f qcow2 -o preallocation=off "$QEMU_HDA" "${QEMU_HDA_SIZE:-8G}"
		)
	fi

	# http://wiki.qemu.org/download/qemu-doc.html#Invocation
	qemuScsiDevice='virtio-scsi-pci'
	case "$qemuArch" in
		arm) qemuScsiDevice='virtio-scsi-device' ;;
	esac

	#qemuArgs+=( -hda "$QEMU_HDA" )
	#qemuArgs+=( -drive file="$QEMU_HDA",index=0,media=disk,discard=unmap )
	qemuArgs+=(
		-drive file="$QEMU_HDA",format=qcow2,if=none,id=drive-sata0-0-0,cache=directsync 
		-device ide-hd,bus=ide.0,drive=drive-sata0-0-0,id=sata0-0-0,bootindex=1,write-cache=off 
#		-drive file="$QEMU_HDA",index=0,media=disk,discard=unmap,detect-zeroes=unmap,if=none,id=hda
#		-device "$qemuScsiDevice"
#		-device scsi-hd,drive=hda
	)
fi

if [ -n "${QEMU_CDROM:-}" ]; then
	#qemuArgs+=( -cdrom file=${QEMU_CDROM},format=raw,if=none,id=drive-sata0-0-1,readonly=on -device ide-cd,bus=ide.1,drive=drive-sata0-0-1,id=sata0-0-1  )
	qemuArgs+=( -cdrom ${QEMU_CDROM} )
fi

if [ -n "${QEMU_BOOT:-}" ]; then
	qemuArgs+=( -boot "$QEMU_BOOT" )
fi

netArg='user'
netArg+=",hostname=$(hostname)"
if [ -n "${QEMU_NET_USER_EXTRA:-}" ]; then
	netArg+=",$QEMU_NET_USER_EXTRA"
fi
for port in "${qemuPorts[@]}"; do
	netArg+=",hostfwd=tcp::$port-:$port"
	netArg+=",hostfwd=udp::$port-:$port"
done

qemuNetDevice='virtio-net-pci'
case "$qemuArch" in
	arm) qemuNetDevice='virtio-net-device' ;;
esac

qemuArgs+=(
	-netdev "$netArg,id=net"
	-device "$qemuNetDevice,netdev=net"
	#-cpu EPYC-IBPB,x2apic=on,tsc-deadline=on,hypervisor=on,tsc-adjust=on,clwb=on,umip=on,stibp=on,arch-capabilities=on,ssbd=on,xsaves=on,cmp-legacy=on,perfctr-core=on,clzero=on,wbnoinvd=on,amd-ssbd=on,virt-ssbd=on,rdctl-no=on,skip-l1dfl-vmentry=on,mds-no=on,monitor=off,hv-time,hv-relaxed,hv-vapic,hv-spinlocks=0x1fff
	#-device pcie-root-port,port=0x10,chassis=1,id=pci.1,bus=pcie.0,multifunction=on,addr=0x2
	#-device pcie-root-port,port=0x11,chassis=2,id=pci.2,bus=pcie.0,addr=0x2.0x1
	#-device pcie-root-port,port=0x12,chassis=3,id=pci.3,bus=pcie.0,addr=0x2.0x2
	#-device pcie-root-port,port=0x13,chassis=4,id=pci.4,bus=pcie.0,addr=0x2.0x3
	#-device pcie-root-port,port=0x14,chassis=5,id=pci.5,bus=pcie.0,addr=0x2.0x4
	#-device qemu-xhci,p2=15,p3=15,id=usb,bus=pci.2,addr=0x0
	#-device virtio-vga,id=video0,virgl=on,max_outputs=1
	#-device ich9-intel-hda,id=sound0,bus=pcie.0,addr=0x1b
	#-device virtio-balloon-pci,id=balloon0,bus=pci.4,addr=0x0
	-vnc ':0'
	-vga 'virtio'
	#-display 'sdl,gl=on'
)
if [ -z "${QEMU_NO_SERIAL:-}" ]; then
	qemuArgs+=(
		-serial stdio
	)
fi
qemuArgs+=( "$@" )

set -x
exec "$qemu" "${qemuArgs[@]}"
